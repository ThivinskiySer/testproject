package ua.ck.myProject.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ua.ck.myProject.entities.Role;
import ua.ck.myProject.entities.User;
import ua.ck.myProject.service.UserService;

import java.util.ArrayList;
import java.util.List;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = userService.getUserByLogin(login);
        List<GrantedAuthority> roles = getGrantedAuthorities(getRolesNames(user.getRoles()));
        return new org.springframework.security.core.userdetails.User(
                login,
                user.getPassword(),
                roles);
    }

    private List<String> getRolesNames(List<Role> roles) {
        List<String> rolesAsList = new ArrayList<>();
        for (Role role : roles) {
            rolesAsList.add(role.getName());
        }
        return rolesAsList;
    }

    private List<GrantedAuthority> getGrantedAuthorities(List<String> roles) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
        return authorities;
    }
}


