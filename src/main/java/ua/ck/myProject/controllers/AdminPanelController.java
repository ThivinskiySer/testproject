package ua.ck.myProject.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ua.ck.myProject.service.UserService;

import java.util.List;

@Controller
public class AdminPanelController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/getRegisteredUsers", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getRegisteredUsers() {
        return userService.getAllUsersLogin();
    }
}

