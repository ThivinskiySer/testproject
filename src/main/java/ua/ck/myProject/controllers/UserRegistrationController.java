package ua.ck.myProject.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ua.ck.myProject.entities.Role;
import ua.ck.myProject.entities.User;
import ua.ck.myProject.service.RoleService;
import ua.ck.myProject.service.UserService;

import java.util.ArrayList;

@Controller
public class UserRegistrationController {

    @Autowired
    UserService userService;
    @Autowired
    RoleService roleService;

    @RequestMapping(value = "/registration", method = {RequestMethod.GET, RequestMethod.POST})
    public String registration() {
        return "registration";
    }

    @RequestMapping(value = "/registerUser", method = RequestMethod.POST)
    public String registerUser(@RequestParam String login, @RequestParam String password) {
        User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        user.setRoles(new ArrayList<Role>() {{
            add(roleService.getRole(1));
        }});
        userService.saveUser(user);
        return "redirect:login";
    }
}
