package ua.ck.myProject.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.ck.myProject.entities.Role;

@Repository
public class RoleDAO {

    @Autowired
    SessionFactory sessionFactory;

    public Role getRole(Integer id) {
        return (Role) sessionFactory.getCurrentSession().get(Role.class, id);
    }
}
