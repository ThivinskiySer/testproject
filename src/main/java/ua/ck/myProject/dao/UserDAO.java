package ua.ck.myProject.dao;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.ck.myProject.entities.User;

import java.util.List;

@Repository
public class UserDAO {

    @Autowired
    SessionFactory sessionFactory;

    public void saveUser(User user) {
        sessionFactory.getCurrentSession().saveOrUpdate(user);
    }

    public boolean isLoginFree(String login) {
        return sessionFactory.getCurrentSession().createCriteria(User.class)
                .add(Restrictions.eq("login", login)).list().isEmpty();
    }

    public User getUserByLogin(String login) {
        return (User) sessionFactory.getCurrentSession().createCriteria(User.class)
                .add(Restrictions.eq("login", login)).uniqueResult();
    }

    public List<String> getAllUsersLogin() {
        return sessionFactory.getCurrentSession()
                .createCriteria(User.class).setProjection(Projections.property("login")).list();
    }
}
