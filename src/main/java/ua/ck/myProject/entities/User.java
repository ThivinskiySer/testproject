package ua.ck.myProject.entities;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "User")
public class User {

    @Id
    @GeneratedValue
    @Column(name = "IdUser")
    private int id;

    @Column(name = "Login")
    private String login;

    @Column(name = "Password")
    private String password;

    public String getLogin() {
        return login;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_has_role",
            joinColumns = @JoinColumn(name = "User_idUser"),
            inverseJoinColumns = @JoinColumn(name = "Role_idRole")
    )
    private List<Role> roles;

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}