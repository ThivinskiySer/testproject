package ua.ck.myProject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.ck.myProject.dao.RoleDAO;
import ua.ck.myProject.entities.Role;

import javax.transaction.Transactional;

@Service
@Transactional
public class RoleService {

    @Autowired
    private RoleDAO roleDAO;

    public Role getRole(Integer id) {
        return roleDAO.getRole(id);
    }
}
