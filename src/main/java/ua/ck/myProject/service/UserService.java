package ua.ck.myProject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.ck.myProject.dao.UserDAO;
import ua.ck.myProject.entities.User;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class UserService {

    @Autowired
    private UserDAO userDAO;

    public void saveUser(User user) {
        userDAO.saveUser(user);
    }

    public boolean isLoginFree(String login) {
        return userDAO.isLoginFree(login);
    }

    public User getUserByLogin(String login) {
        return userDAO.getUserByLogin(login);
    }

    public List<String> getAllUsersLogin() {
        return userDAO.getAllUsersLogin();
    }
}
