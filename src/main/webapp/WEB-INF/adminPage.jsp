<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script type="text/javascript" src="../js/adminPanel.js"></script>
</head>
<body>
Hello admin | <a href="/exit">Logout</a><br/>
<button id="getRegisteredUsers">Get registered users</button>
<button id="clearRegisteredUsersList">Clear users list</button>
<div id="registered-users-list"></div>
</body>
</html>
