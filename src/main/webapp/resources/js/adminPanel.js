var adminPanel = {

     _usersListDiv: null,
     _usersList: null,

    init: function () {
        this._usersListDiv = $("#registered-users-list");
        this._usersList = document.createElement("p");
        $("#getRegisteredUsers").bind('click', this.printRegisteredUsers);
        $("#clearRegisteredUsersList").bind('click', this.clearRegisteredUsersList);
    },

    printRegisteredUsers: function (ths) {
        $.ajax({
            type: "GET",
            url: "getRegisteredUsers",
            success: function (logins) {
                var usersListAsString = "";
                $(logins).each(function (i, login) {
                    usersListAsString += login;
                    i < logins.length - 1 ? usersListAsString += ", " : usersListAsString += "."
                });
                with (adminPanel) {
                    _usersList.innerText = usersListAsString;
                    _usersListDiv.append(_usersList);
                }
            }
        });
    },

    clearRegisteredUsersList: function () {
        with (adminPanel) {
            _usersList.innerText = "";
        }
    }
};
document.addEventListener("DOMContentLoaded", function () {
    adminPanel.init();
});
